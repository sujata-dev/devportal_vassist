from rasa_core.channels import HttpInputChannel
from rasa_core.agent import Agent
from rasa_core.interpreter import RasaNLUInterpreter
from ras_connectory import SlackInput


nlu_interpreter = RasaNLUInterpreter('projects/default/model')
agent = Agent.load('models/dialogue', interpreter = nlu_interpreter)

input_channel = SlackInput('xoxp-338354132598-337498755954-337625137363-1b9d34da65ec2f756b4017cc39bf20d9', #app verification token
							'xoxb-337661670564-qWa0xHrKBkLUzZG3rcsixYAX', # bot verification token
							'6zaqqlbYI6BLWioG1jpRPcSx', # slack verification token
							True)

agent.handle_channel(HttpInputChannel(5001, '/', input_channel))