from rasa_core.actions import Action
from rasa_core.events import SlotSet, TopicSet, AllSlotsReset
from rasa_core.events import Restarted
from SlotFill_Compulsory import SlotFill_Compulsory
from SlotFill_Optional import SlotFill_Optional


class ActionProvisionStatus(Action):
    def name(self):
        return 'action_provision_status'

    def run(self, dispatcher, tracker, domain):
        slot_list_compulsory = ["envName"]

        obj = SlotFill_Compulsory(slot_list_compulsory)
        obj.slot_checker(dispatcher, tracker, domain)
        
        if tracker.get_slot("compulsory_matches") == True:
            slot_list_optional = ["temp", "provisionRequestID"]
            obj1 = SlotFill_Optional(slot_list_optional)
            obj1.slot_checker(dispatcher, tracker, domain)


        if tracker.get_slot("compulsory_matches") == True and tracker.get_slot("optional_matches") == True:
            if tracker.get_slot("provisionRequestID") is None:
                dispatcher.utter_template("utter_provisionStatus_template", x=tracker.get_slot("temp"), y=tracker.get_slot("envName"))
            elif tracker.get_slot("temp") is None:
                dispatcher.utter_template("utter_provisionStatus_request", x=tracker.get_slot("provisionRequestID"), y=tracker.get_slot("envName"))
            else:
                dispatcher.utter_template("utter_provisionStatus_both", x= tracker.get_slot("temp"), y=tracker.get_slot("provisionRequestID"), z=tracker.get_slot("envName"))
            return [AllSlotsReset()]

        return []
