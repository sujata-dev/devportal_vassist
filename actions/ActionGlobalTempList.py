from rasa_core.actions import Action
from rasa_core.events import SlotSet, TopicSet, AllSlotsReset
from rasa_core.events import Restarted
from SlotFill_Compulsory import SlotFill_Compulsory


class ActionGlobalTempList(Action):
    def name(self):
        return 'action_global_temp_list'

    def run(self, dispatcher, tracker, domain):
        slot_list_compulsory = ["envName"]

        #slot_list = domain.required_slots['request_status']
        obj = SlotFill_Compulsory(slot_list_compulsory)
        obj.slot_checker(dispatcher, tracker, domain)

        if tracker.get_slot("compulsory_matches") == True:
            dispatcher.utter_template("utter_globalTemplateList", x=tracker.get_slot("envName"))
        return []
