from rasa_core.actions import Action
from rasa_core.events import SlotSet, TopicSet
from rasa_core.events import Restarted

required_key = None
class SlotFill_Compulsory:
    slot_list = []

    def __init__(self, slot_list):
        self.slot_list = slot_list

    def slot_checker(self, dispatcher, tracker, domain):
        tracker.update(SlotSet("compulsory_matches", False))

        for recent_key in self.slot_list:
            if next(tracker.get_latest_entity_values(recent_key), None):
                break
            else:
                recent_key = None

        for slot_key in self.slot_list:
            global required_key
            if recent_key and recent_key != required_key:

                dispatcher.utter_template("utter_confirm", x=recent_key,
                                          y=tracker.get_slot(recent_key))
                recent_key = None

            if tracker.get_slot(slot_key) is not None:
                tracker.update(SlotSet("compulsory_matches", True))
                continue

            else:
                required_key=slot_key
                tracker.update(SlotSet("compulsory_matches", False))
                text = "utter_" + slot_key
                if text in dispatcher.domain.templates.keys():
                    dispatcher.utter_template(text)

                else:
                    dispatcher.utter_template("utter_default", x=slot_key)
                break
