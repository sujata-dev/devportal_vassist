from rasa_core.actions import Action
from rasa_core.events import SlotSet, TopicSet, AllSlotsReset
from rasa_core.events import Restarted
from SlotFill_Compulsory import SlotFill_Compulsory


class ActionProvisionList(Action):
    def name(self):
        return 'action_provision_list'

    def run(self, dispatcher, tracker, domain):
        slot_list_compulsory = ["envName", "temp"]
        #slot_list_optional = ["tempID", "tempName"]

        obj = SlotFill_Compulsory(slot_list_compulsory)
        obj.slot_checker(dispatcher, tracker, domain)

        if tracker.get_slot("compulsory_matches") == True:
            dispatcher.utter_template("utter_provisionList", x=tracker.get_slot("temp"), y=tracker.get_slot("envName"))

        return []
