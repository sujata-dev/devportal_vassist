import os

from rasa_core import utils
from rasa_core.conversation import Topic
from rasa_core.domain import TemplateDomain
from rasa_core.utils import read_yaml_file



class CustomTemplate(TemplateDomain):

    def __init__(self, intents, entities, slots, templates, action_names,
                 action_factory, topics, required_slots, **kwargs):
        TemplateDomain.__init__(self,intents, entities, slots, templates, action_names, action_factory, topics, **kwargs)
        self._required_slots = required_slots


    @classmethod
    def load(cls, file_name, action_factory=None):



        if not os.path.isfile(file_name):
            raise Exception(
                    "Failed to load domain specification from '{}'. "
                    "File not found!".format(os.path.abspath(file_name)))

        cls.validate_domain_yaml(file_name)
        data = read_yaml_file(file_name)
        utter_templates = cls.collect_templates(data.get("templates", {}))
        if not action_factory:
            action_factory = data.get("action_factory", None)
        topics = [Topic(name) for name in data.get("topics", [])]
        slots = cls.collect_slots(data.get("slots", {}))
        required_slots = cls.collect_required_slots(data.get("required_slots", {}))
        additional_arguments = data.get("config", {})

        return CustomTemplate(
                data.get("intents", []),
                data.get("entities", []),
                slots,
                utter_templates,
                data.get("actions", []),
                action_factory,
                topics,
                required_slots,
                **additional_arguments
        )

    @staticmethod
    def collect_required_slots(required_slots_dict):
        # type: (Dict[Text, List[Any]]) -> Dict[Text, List[Dict[Text, Any]]]
        """Go through the templates and make sure they are all in dict format"""

        required_slots = {}
        for intent, slots in required_slots_dict.items():
            slots_list = []
            for slot in slots:
                # templates can either directly be strings or a dict with
                # options we will always create a dict out of them
                slots_list.append(slot)
            required_slots[intent] = slots_list
        return required_slots

    @utils.lazyproperty
    def required_slots(self):
        return self._required_slots