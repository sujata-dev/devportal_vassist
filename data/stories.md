## Generated Story 5441538340371847535
* _greet
    - utter_greet
## Generated Story -3474425044257025053
* templateList
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": false}
    - action_temp_list
* templateList{"userName": "xyz"}
    - slot{"userName": "xyz"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"compulsory_matches": false}
    - action_temp_list
* templateList{"envName": "prod"}
    - slot{"envName": "prod"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"compulsory_matches": true}
    - action_temp_list
* greet
    - utter_greet
    - export
## Generated Story 1852477192142073889
* globalTemplateList{"envName": "prod"}
    - slot{"envName": "prod"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - action_global_temp_list
* templateList{"envName": "dev"}
    - slot{"envName": "dev"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": false}
    - action_temp_list
* templateList{"userName": "abcxyz"}
    - slot{"userName": "abcxyz"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"compulsory_matches": true}
    - action_temp_list
* greet
    - utter_greet
* globalTemplateList
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - action_global_temp_list
    - export
## Generated Story 7156176189473437628
* provisionList
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": false}
    - action_provision_list
* templateList{"envName": "dev"}
    - slot{"envName": "dev"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"compulsory_matches": false}
    - action_provision_list
* provisionStatus{"DATE": "109273", "temp": "109273"}
    - slot{"temp": "109273"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"compulsory_matches": true}
    - action_provision_list
* greet
    - utter_greet
* globalTemplateList
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - action_global_temp_list
* templateList
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": false}
    - action_temp_list
* globalTemplateList{"userName": "xxy"}
    - slot{"userName": "xxy"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"compulsory_matches": true}
    - action_temp_list
* provisionList{"envName": "prod", "CARDINAL": "423533", "temp": "423533"}
    - slot{"envName": "prod"}
    - slot{"temp": "423533"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"compulsory_matches": true}
    - action_provision_list
    - export
## Generated Story -1613215795151129289
* provisionStatus
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": false}
    - action_provision_status
* templateList{"envName": "dev"}
    - slot{"envName": "dev"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"optional_matches": false}
    - slot{"optional_matches": false}
    - action_provision_status
* fetchResources{"provisionRequestID": "32353"}
    - slot{"provisionRequestID": "32353"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"optional_matches": false}
    - slot{"optional_matches": true}
    - action_provision_status
    - reset_slots
* greet
    - utter_greet
* globalTemplateList
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": false}
    - action_global_temp_list
* globalTemplateList{"envName": "qa"}
    - slot{"envName": "qa"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - action_global_temp_list
* templateList
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": false}
    - action_temp_list
* templateList{"userName": "aaa"}
    - slot{"userName": "aaa"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"compulsory_matches": true}
    - action_temp_list
* greet
    - utter_greet
    - export
## Generated Story -7641518598203738202
* fetchResources
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": false}
    - action_fetch_resources
* templateList{"envName": "dev"}
    - slot{"envName": "dev"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"optional_matches": false}
    - slot{"optional_matches": false}
    - action_fetch_resources
* provisionStatus{"temp": "abc"}
    - slot{"temp": "abc"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"optional_matches": false}
    - slot{"optional_matches": true}
    - action_fetch_resources
    - reset_slots
* provisionStatus{"DATE": "34332", "envName": "prod", "CARDINAL": "23523", "provisionRequestID": "34332", "temp": "23523"}
    - slot{"provisionRequestID": "34332"}
    - slot{"temp": "23523"}
    - slot{"envName": "prod"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"optional_matches": false}
    - slot{"optional_matches": true}
    - action_provision_status
    - reset_slots
* globalTemplateList
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": false}
    - action_global_temp_list
* templateList{"envName": "dev"}
    - slot{"envName": "dev"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - action_global_temp_list
* templateList
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": false}
    - action_temp_list
* templateList{"userName": "abc@vmtest.com"}
    - slot{"userName": "abc@vmtest.com"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"compulsory_matches": true}
    - action_temp_list
    - export
## Generated Story -8489158793503710278
* fetchResources{"CARDINAL": "423252", "provisionRequestID": "325325", "temp": "423252"}
    - slot{"provisionRequestID": "325325"}
    - slot{"temp": "423252"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": false}
    - action_fetch_resources
* templateList{"envName": "dev"}
    - slot{"envName": "dev"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"optional_matches": false}
    - slot{"optional_matches": true}
    - action_fetch_resources
    - reset_slots
* provisionList{"DATE": "32523", "temp": "32523"}
    - slot{"temp": "32523"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": false}
    - action_provision_list
* templateList{"envName": "dev"}
    - slot{"envName": "dev"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"compulsory_matches": true}
    - action_provision_list
* templateList{"userName": "abc", "envName": "prod"}
    - slot{"userName": "abc"}
    - slot{"envName": "prod"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"compulsory_matches": true}
    - action_temp_list
* provisionStatus{"DATE": "template 12433", "envName": "dev", "provisionRequestID": "testaaa", "temp": "12433"}
    - slot{"temp": "12433"}
    - slot{"provisionRequestID": "testaaa"}
    - slot{"envName": "dev"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"optional_matches": false}
    - slot{"optional_matches": true}
    - action_provision_status
    - reset_slots
* globalTemplateList
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": false}
    - action_global_temp_list
* globalTemplateList{"envName": "qa"}
    - slot{"envName": "qa"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - action_global_temp_list
* greet
    - utter_greet
    - export
## Generated Story 1445438670629795492
* fetchResources
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": false}
    - action_fetch_resources
* globalTemplateList{"envName": "qa"}
    - slot{"envName": "qa"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"optional_matches": false}
    - slot{"optional_matches": false}
    - action_fetch_resources
* provisionStatus{"temp": "testforapi"}
    - slot{"temp": "testforapi"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"optional_matches": false}
    - slot{"optional_matches": true}
    - action_fetch_resources
    - reset_slots
* provisionList
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": false}
    - action_provision_list
* templateList{"envName": "dev"}
    - slot{"envName": "dev"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"compulsory_matches": false}
    - action_provision_list
* provisionStatus{"temp": "abc"}
    - slot{"temp": "abc"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"compulsory_matches": true}
    - action_provision_list
* provisionStatus
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"optional_matches": false}
    - slot{"optional_matches": true}
    - action_provision_status
    - reset_slots
* greet
    - utter_greet
    - export
## Generated Story 7978975805181875218
* fetchResources{"DATE": "252452", "envName": "dev", "CARDINAL": "35235", "provisionRequestID": "252452", "temp": "35235"}
    - slot{"temp": "35235"}
    - slot{"provisionRequestID": "252452"}
    - slot{"envName": "dev"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"optional_matches": false}
    - slot{"optional_matches": true}
    - action_fetch_resources
    - reset_slots
* provisionStatus{"DATE": "233522", "envName": "dev", "provisionRequestID": "233522", "temp": "32523"}
    - slot{"temp": "32523"}
    - slot{"provisionRequestID": "233522"}
    - slot{"envName": "dev"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"optional_matches": false}
    - slot{"optional_matches": true}
    - action_provision_status
    - reset_slots
* greet
    - utter_greet
    - export
## Generated Story -8387814610938179460
* provisionList
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": false}
    - action_provision_list
* templateList{"envName": "dev"}
    - slot{"envName": "dev"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"compulsory_matches": false}
    - action_provision_list
* provisionStatus{"DATE": "43252", "temp": "43252"}
    - slot{"temp": "43252"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"compulsory_matches": true}
    - action_provision_list
* provisionStatus
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"optional_matches": false}
    - slot{"optional_matches": true}
    - action_provision_status
    - reset_slots
* fetchResources
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": false}
    - action_fetch_resources
* templateList{"envName": "dev"}
    - slot{"envName": "dev"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"optional_matches": false}
    - slot{"optional_matches": false}
    - action_fetch_resources
* provisionList{"CARDINAL": "45233", "temp": "45233"}
    - slot{"temp": "45233"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"optional_matches": false}
    - slot{"optional_matches": true}
    - action_fetch_resources
    - reset_slots
* greet
    - utter_greet
    - export
## Generated Story -2958200214048595451
* templateList{"userName": "xxx@vmware.com", "envName": "qa"}
    - slot{"userName": "xxx@vmware.com"}
    - slot{"envName": "qa"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"compulsory_matches": true}
    - action_temp_list
* provisionList{"envName": "dev", "CARDINAL": "111", "temp": "111"}
    - slot{"temp": "111"}
    - slot{"envName": "dev"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"compulsory_matches": true}
    - action_provision_list
* globalTemplateList{"envName": "dev"}
    - slot{"envName": "dev"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - action_global_temp_list
* provisionList{"PERSON": "dev", "envName": "dev", "temp": "mytemplate"}
    - slot{"temp": "mytemplate"}
    - slot{"envName": "dev"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"compulsory_matches": true}
    - action_provision_list
* greet
    - utter_greet
* globalTemplateList{"envName": "prod"}
    - slot{"envName": "prod"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - action_global_temp_list
* templateList{"userName": "abc"}
    - slot{"userName": "abc"}
    - slot{"compulsory_matches": false}
    - slot{"compulsory_matches": true}
    - slot{"compulsory_matches": true}
    - action_temp_list
    - export
